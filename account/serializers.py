from rest_framework import serializers
from .models import Account


class AccountSerializer(serializers.Serializer):
    initial_user_type=serializers.CharField(choices=INTIAL_USER_TYPE,max_length=15)
    user = serializers.OneToOneField(User, on_delete=serializers.CASCADE)
    user_type=serializers.CharField(choices=INTIAL_USER_TYPE,max_length=15)
    first_name = serializers.CharField(max_length=255)
    middle_name = serializers.CharField(max_length=255, blank=True, null=True)
    last_name = serializers.CharField(max_length=255)
    img=serializers.ImageField(upload_to='ac_profile')
    ardc_no = serializers.CharField(max_length=12)
    document=serializers.OneToOneField(Document,on_delete=serializers.CASCADE,null=True,blank=True)
    address_line1=serializers.TextField()
    address_line2=serializers.TextField()
    landmark=serializers.TextField()
    zip=serializers.IntegerField(max_length=6)
    city=serializers.CharField(max_length=20)
    state=serializers.CharField(max_length=20)
    country=serializers.CharField(max_length=20)

    def create(self,validated_data):
        print(self,'--',validated_data)
        return validated_data


    